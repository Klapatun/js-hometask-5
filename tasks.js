'use strict';
/*
    В этом задании нельзя менять разметку, только писать код в этом файле.
 */

/**
*   1. Удали со страницы элемент с id "deleteMe"
**/

function removeBlock() {
  const deleteElement = document.getElementById('deleteMe');
  deleteElement.remove();
}

/**
 *  2. Сделай так, чтобы во всех элементах с классом wrapper остался только один параграф,
 *  в котором будет сумма чисел из всех параграфов.
 *
 *  Например, такой элемент:
 *
 *  <div class="wrapper"><p>5</p><p>15</p><p>25</p><p>35</p></div>
 *
 *  должен стать таким
 *
 *  <div class="wrapper"><p>80</p></div>
 */

function calcParagraphs() {
  const wrapperClass = document.getElementsByClassName('wrapper');
  for (let wrapperIterator of wrapperClass) {
    const tagP = wrapperIterator.getElementsByTagName('p');
    let sum = 0;
    while(tagP.item(0)) {
      sum += +tagP.item(0).textContent;
      tagP.item(0).remove();
    }
    const text = document.createElement('p');
    text.textContent = `${sum}`;
    wrapperIterator.appendChild(text);
    
  }
  
}

/**
 *  3. Измени value и type у input c id "changeMe"
 *
 */

function changeInput() {
  const whatNeedToChange = document.getElementById('changeMe');
  whatNeedToChange.value = 'qwerty123';
  whatNeedToChange.type = 'text';
}

/**
 *  4. Используя функции appendChild и insertBefore дополни список с id "changeChild"
 *  чтобы получилась последовательность <li>0</li><li>1</li><li>2</li><li>3</li>
 *
 */

function appendList() {
  const ulBlock = document.getElementById('changeChild')
  let elem = document.createElement('li');
  elem.textContent = '1';
  ulBlock.insertBefore(elem, ulBlock.children[1]);
  elem = document.createElement('li');
  elem.textContent = '3';
  ulBlock.appendChild(elem);
  elem = document.createElement('li');
  elem.textContent = '4';
  ulBlock.appendChild(elem);
}

/**
 *  5. Поменяй цвет всем div с class "item".
 *  Если у блока class "red", то замени его на "blue" и наоборот для блоков с class "blue"
 */

function changeColors() {
  const items = document.getElementById('blocks').children;
  for(let item of items) {
    let changeEl = document.createElement('div');

    if(item.className.includes('red')) {
      changeEl.className = 'item blue'
      item.parentElement.replaceChild(changeEl, item);
    }
    else {
      changeEl.className = 'item red'
      item.parentElement.replaceChild(changeEl, item);
    }
  }
}

removeBlock();
calcParagraphs();
changeInput();
appendList();
changeColors();
